package demojengradle;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class TestCalculadora {

	@Test
	public void testSumar() {
		System.out.println("testSumar");
		double input1=12;
		double input2=14;
		double esperado=26;
		assertEquals(esperado, Calculadora.sumar(input1, input2));
	}
	
	@Test
	public void testRestar() {
		System.out.println("testRestar");
		double input1=12;
		double input2=14;
		double esperado=-2;
		assertEquals(esperado, Calculadora.restar(input1, input2));
	}
	
	@Test
	public void testMultiplicar() {
		System.out.println("testMultiplicar");
		double input1=2;
		double input2=4;
		double esperado=8;
		assertEquals(esperado, Calculadora.multiplicar(input1, input2));
	}
	
	@Test
	public void testDividir() {
		System.out.println("testDividir");
		double input1=12;
		double input2=4;
		double esperado=3;
		assertEquals(esperado, Calculadora.dividir(input1, input2));
	}
}